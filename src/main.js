// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import 'babel-polyfill'
import App from './App'
import router from './router'
import {store} from './store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css' // Ensure you are using css-loader
import Vuex from 'vuex'
import * as VueGoogleMaps from 'vue2-google-maps'
import GlobalComponents from './globalComponents.js'
import VuetifyGoogleAutocomplete from 'vuetify-google-autocomplete';

const fb = require('@/firebaseConfig.js')
var googlekey = 'AIzaSyDbzk7TqaT0vwUgU-_6T4o0S4BzN9VQtBk';
Vue.config.productionTip = false
Vue.use(Vuetify)
Vue.use(Vuex)
Vue.use(GlobalComponents)
Vue.use(VueGoogleMaps, {
  load: {
    key: googlekey,
    libraries: 'places', // This is required if you use the Autocomplete plugin
  },
})
Vue.use(VuetifyGoogleAutocomplete, {
  apiKey: googlekey, // Can also be an object. E.g, for Google Maps Premium API, pass `{ client: <YOUR-CLIENT-ID> }`
});

var app;
fb.auth.onAuthStateChanged(user => {
  if(!app){
    app = new Vue({
      el: '#app',
      router,
      store,
      components: { App },
      template: '<App/>'
    })
  }
})