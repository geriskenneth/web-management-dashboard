import firebase from 'firebase'
import 'firebase/firestore'

const config = {
    apiKey: "AIzaSyBefclCYlkl0vIolwgRqP0bzVjLly5uaU8",
    authDomain: "site-manager-demo.firebaseapp.com",
    databaseURL: "https://site-manager-demo.firebaseio.com",
    projectId: "site-manager-demo",
    storageBucket: "site-manager-demo.appspot.com",
    messagingSenderId: "406484383235"
  };
firebase.initializeApp(config)

// firebase utils
const db = firebase.firestore()
const auth = firebase.auth()
const currentUser = auth.currentUser

// date issue fix according to firebase
const settings = {
    timestampsInSnapshots: true
}
db.settings(settings)

// // firebase collections
const clientsCollection = db.collection('Clients')
const usersCollection = db.collection('Users')
const websitesCollection = db.collection('Websites')
const accountsCollection = db.collection('Accounts')
const countersCollection = db.collection('Counters')
const aliasesCollection = db.collection('Aliases')
const emailsCollection = db.collection('Emails')
const formsCollection = db.collection('Forms')

export {
    db,
    auth,
    currentUser,
    usersCollection,
    clientsCollection,
    websitesCollection,
    accountsCollection,
    countersCollection,
    aliasesCollection,
    formsCollection,
    emailsCollection
}
