import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import firebase from 'firebase'

/*USER AUTHENTICATION */
import Login from '@/components/Auth/Login'

/*WEBSITES*/
import websiteIndex from '@/components/Websites'
import websiteDetails from '@/components/Websites/websiteDetails'

/*Client*/
import clientIndex from '@/components/Clients'
import profileIndex from '@/components/Clients/Profile'
import profileWebsites from '@/components/Clients/Profile/websites'

/**MY ACCOUNTs */
import accountIndex from '@/components/OnlineAccounts'

/*EBLASTS */
import eblastIndex from '@/components/Eblast'

/*ADMIN */
import adminIndex from '@/components/Admin'

/*FORM BUILDER */
import formBuilder from '@/components/formBuilder'


/*ROUTER*/
Vue.use(Router)
let router = new Router({
  mode: 'history',
  linkActiveClass: 'active',
  routes: [
    {
      path: '*',
      redirect: '/user/login'
    },
    {
      path: '/',
      name: 'Home',
      component: Home,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/user/login',
      name: 'Login',
      component: Login,
    },
    {
      path: '/websites',
      name: 'websiteIndex',
      component: websiteIndex,
      meta: {
        requiresAuth: true,
      },
    },
    {
      path: '/websites/:project',
      name: 'websiteDetails',
      component: websiteDetails,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/clients',
      name: 'clientIndex',
      component: clientIndex,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/clients/:client',
      name: 'profile-index',
      component: profileIndex,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/clients/:client/websites', 
      name: 'profile-websites', 
      component: profileWebsites,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/my-accounts/',
      name: 'account-index',
      component: accountIndex,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/eblasts/',
      name: 'eblast-index',
      component: eblastIndex,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/admin',
      name: 'admin',
      component: adminIndex,
      meta: {
        requiresAuth: true,
      }
    },
    {
      path: '/formbuilder',
      name: 'formbuilder',
      component: formBuilder,
      meta: {
        requiresAuth: true,
      }
    }
  ],
  scrollBehavior (to, from, savedPosition) {
    if (to.hash) {
      return {selector: to.hash}
    } else if (savedPosition) {
      return savedPosition
    } else {
      return { x: 0, y: 0 }
    }
  }
})

router.beforeEach((to, from, next) => {
  let currentUser = firebase.auth().currentUser;
  let requiresAuth = to.matched.some(record => record.meta.requiresAuth);
  if (requiresAuth && !currentUser) next('login')
  else if (!requiresAuth && currentUser) next('/websites')
  else next()
})

export default router

