import Vue from 'vue'
import Vuex from 'vuex';
const fb = require('../firebaseConfig.js')

Vue.use(Vuex)

// handle page reload
fb.auth.onAuthStateChanged(user => {
    if (user) {
        store.commit('setCurrentUser', user)
        store.dispatch('fetchUserProfile')
        store.dispatch('fetchClients')
        store.dispatch('fetchWebsites')
        store.dispatch('fetchAccounts')
        store.dispatch('fetchWebsiteEmails')
        // store.dispatch('fetchClientDetails')

        fb.usersCollection.doc(user.uid).onSnapshot(doc => {
            store.commit('setUserProfile', doc.data())
        })
  
    }
})

export const store = new Vuex.Store({
    state: {
        currentUser: null,
        userProfile: {},
        clientDetails: {},
        websiteDetails: {},
        websiteOverview: {},
        clients: [],
        websites: [],
        accounts: [],
        websiteAliases: [],
        domainAliases: [],
        websiteEmails: [],
        forms: []
    },
    actions: {
        /** USER ACTIONS */
        updateProfile({ commit, state }, data) {
            let name = data.name
            let status = data.status
            fb.usersCollection.doc(state.currentUser.uid).update({ name, status });
        },
        fetchUserProfile({ commit, state }) {
            fb.usersCollection.doc(state.currentUser.uid).get().then(res => {
                commit('setUserProfile', res.data())
            }).catch(err => {
                console.log(err)
            })
        },
        clearData({ commit }) {
            commit('setCurrentUser', null)
            commit('setUserProfile', {})
            commit('setClientDetails', {})
            commit('setClients', null)
            commit('setWebsites', null)
        },

        /**CLIENT ACTIONS */
        fetchClientDetails({ commit, state }, clientId) {
                fb.clientsCollection.doc(clientId).get().then(res => {
                    commit('setClientDetails', res.data());
                }).catch(err => {
                    console.log(err)
                })
        },
        fetchWebsiteOverview({ commit, state }, data) {
            fb.websitesCollection
            .get()
            .then(querySnapshot => {
                let websiteOverviewArray = [];
                querySnapshot.forEach(doc => {
                    var test = doc.data().builder
                    test.forEach(builder =>{
                        let client = doc.data();
                        if(builder == data.clientId && client.status == true){
                            websiteOverviewArray.push(client);
                        }
                    })
                });
                store.commit('setWebsiteOverview', websiteOverviewArray)
            })
            .catch(function(error) {
                console.log("Error getting documents: ", error);
            });
        },
        fetchClients({ commit, state }) {
            fb.clientsCollection.orderBy('createdOn', 'desc').onSnapshot(querySnapshot => {
                let clientsArray = {};
                querySnapshot.forEach(doc => {
                    let client = doc.data();
                    let key = client.name;
                    clientsArray[key] = client;
                })
                store.commit('setClients', clientsArray)
            })
        },

        /**WEBSITE ACTIONS */
        fetchWebsites({commit, state}, data){
            fb.websitesCollection.orderBy('createdOn', 'desc').onSnapshot(querySnapshot => {
                    let websitesArray = {}
                querySnapshot.forEach(doc => {
                    let website = doc.data()
                    let key = website.website;
                    websitesArray[key] = website;
                })
                store.commit('setWebsites', websitesArray)
            })      
        },
        fetchWebsiteDetails({ commit, state }, websiteID) {
            fb.websitesCollection.doc(websiteID).get().then(res => {
                commit('setWebsiteDetails', res.data());
            }).catch(err => {
                console.log(err)
            })
        },
        /**ACCOUNTS ACTIONS */
        fetchAccounts({commit, state}, data){
            fb.accountsCollection.orderBy('createdOn', 'desc').onSnapshot(querySnapshot => {
                    let accountsArray = {}
                querySnapshot.forEach(doc => {
                    let accounts = doc.data()
                    let key = accounts.name;
                    accountsArray[key] = accounts;
                })
                store.commit('setAccounts', accountsArray)
            })      
        },     
        fetchDomainAliases({commit, state}, data){
            fb.aliasesCollection.onSnapshot(querySnapshot => {
                let domainAliasesArray = {}
                querySnapshot.forEach(doc => {
                    domainAliasesArray = doc.data();
                })
                store.commit('setDomainAliases', domainAliasesArray)
            })      
        },
        fetchWebsiteEmails({commit, state}, data){
            fb.emailsCollection.onSnapshot(querySnapshot => {
                let websiteEmailsArray = {}
                querySnapshot.forEach(doc => {
                    websiteEmailsArray = doc.data();
                })
                store.commit('setWebsiteEmails', websiteEmailsArray)
            })      
        },
        fetchForms({commit, state}, data){
            fb.formsCollection.onSnapshot(querySnapshot => {
                let formsArray = {}
                querySnapshot.forEach(doc => {
                    console.log(doc.data());
                    formsArray = doc.data();
                    
                })
                store.commit('setForms', formsArray)
            })      
        }      
    },
    mutations: {
        /** USER MUTATIONS */
        setCurrentUser(state, val) {
            state.currentUser = val;
        },
        setUserProfile(state, val) {
            state.userProfile = val
        },

        /** CLIENTS MUTATIONS */        
        setClients(state, val) {
            val ? state.clients = val : state.clients = []
        },
        setClientDetails(state, val) {
            state.clientDetails = val;
        },
        setWebsiteDetails(state, val) {
            state.websiteDetails = val;
        },
        /** WEBSITE MUTATIONS */
        setWebsites(state, val) {
            val ? state.websites = val : state.websites = []
        },
        setWebsiteOverview(state, val){
            val ? state.websiteOverview = val : state.websiteOverview = []
        },
        /** ACCOUNTS MUTATIONS */
        setAccounts(state, val) {
            val ? state.accounts = val : state.accounts = []
            
        },
        setDomainAliases(state, val) {
            val? state.domainAliases = val : state.domainAliases = []
        },
        setWebsiteEmails(state, val) {
            val? state.websiteEmails = val : state.websiteEmails = []
        },
        setForms(state, val) {
            val ? state.forms = val : state.forms = []
        }     
    },
    getters:{
        /** CLIENT GETTERS */
        getClientDetails(state) {
            return state.clientDetails
        },
        getWebsiteDetails(state) {
            return state.websiteDetails
        },
        getClientCount(state){
            return state.clientCount
        },
        getWebsiteOverview(state){
            return state.websiteOverview
        },
        getWebsiteEmails(state){
            return state.websiteEmails
        },
        getDomainAliases(state){
            return state.domainAliases
        },
        getForms(state){
            return state.forms
        }
    }
})