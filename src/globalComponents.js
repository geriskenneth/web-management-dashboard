// stats card
import StatsCard from './components/CustomComponents/StatsCard';
import VerticalTabs from './components/CustomComponents/VerticalTabs';


const GlobalComponents = {
   install(Vue) {
      Vue.component('statsCard', StatsCard);
      Vue.component('verticalTabs', VerticalTabs);
   }
}

export default GlobalComponents
